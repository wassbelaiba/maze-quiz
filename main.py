import pygame
from pygame.locals import *
import choices as ch
from Button import *

'''
    # dimensions de la fenêtre et attributs géneraux
'''
loop = True
screen_x, screen_y = 800, 600
screen = pygame.display.set_mode((screen_x, screen_y), HWSURFACE | DOUBLEBUF)
pygame.display.set_caption("Menu")
icon = pygame.image.load('ico.png')
background = pygame.image.load('bg.jpg')
background = pygame.transform.scale(background, (screen_x, screen_y))
pygame.display.set_icon(icon)
clock = pygame.time.Clock()
'''
 # bouton "Jouer"
'''
green = (0, 255, 0, 0)
dark_green = (69, 152, 39)
play_button_pos_x = 50
play_button_pos_y = screen_y/1.20
play_button = Button(dark_green, play_button_pos_x, play_button_pos_y, "Jouer")

def play():
    global loop 
    loop = False
    pygame.quit()
    ch.run()

'''
    # bouton "Quitter"
'''
red = (255, 0, 0)
dark_red = (185, 72, 72)
exit_button_pos_x = screen_x-230
exit_button_pos_y=play_button_pos_y
exit_button = Button(dark_red, exit_button_pos_x, exit_button_pos_y, "Quitter")


def close():
    global loop
    loop = False


while loop:
    click_detection = False
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            loop = False
        elif event.type == pygame.MOUSEBUTTONUP:
            click_detection = True
    screen.blit(background, (0, 0))
    play_button.display(screen, green, click_detection,play)
    exit_button.display(screen, red, click_detection, close)
    pygame.display.update()
    clock.tick(60)
pygame.quit()


