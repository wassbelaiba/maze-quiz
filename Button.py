import pygame
import time 
# #permet d'initialiser la police de caractère utilisée et plus generalement les fonctionnalites de pygame
pygame.init()


white = (255, 255, 255)
black = (0, 0, 0)
font = pygame.font.SysFont('freesansbold', 32)
click_sound = pygame.mixer.Sound('click.mp3')
class Button:
    def __init__(self, color, x, y, text):
        self.color = color
        self.default_color = color
        self.x = x
        self.y = y
        self.text = text
        self.border_color = black
        self.width = 180
        self.height = 60
    
    def display(self, screen, bright, clicked, func):
        pygame.draw.rect(screen,self.color,(self.x, self.y, self.width, self.height),0,20)
        pygame.draw.rect(screen,self.border_color,(self.x, self.y, self.width, self.height),2,20)
        text = font.render(self.text, True, self.border_color)
        screen.blit(text, (self.x+self.width/3.15, self.y+self.height/3))
        self.update(bright, clicked, func)

    def in_button(self,x,y):
        return self.x<= x<= self.x+self.width and self.y<= y<=  self.y+self.height


    def update(self,bright, clicked, func):
         if pygame.mouse.get_focused():
            x, y = pygame.mouse.get_pos()
            if self.in_button(x,y):
                self.color = bright
                self.border_color = white
                if clicked:
                    click_sound.play()
                    # #permet de faire en sorte que le son soit passe avant que la fenetre ne se ferme
                    time.sleep(0.1)
                    func()
            else:
                self.color = self.default_color
                self.border_color = black




